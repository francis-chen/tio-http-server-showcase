package org.tio.http.server.showcase.model;

import java.io.Serializable;

public class User implements Serializable {

	private static final long	serialVersionUID	= 7038736722168521022L;
	private Integer				id;
	private Short				shortid;
	private Long				longid;
	private Byte				byteid;
	private Float				floatid;
	private Double				doubleid;
	private String				loginname;
	private String				nick;
	private String				ip;

	public Long getLongid() {
		return longid;
	}

	public void setLongid(Long longid) {
		this.longid = longid;
	}

	public Byte getByteid() {
		return byteid;
	}

	public void setByteid(Byte byteid) {
		this.byteid = byteid;
	}

	public Float getFloatid() {
		return floatid;
	}

	public void setFloatid(Float floatid) {
		this.floatid = floatid;
	}

	public Double getDoubleid() {
		return doubleid;
	}

	public void setDoubleid(Double doubleid) {
		this.doubleid = doubleid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLoginname() {
		return loginname;
	}

	public void setLoginname(String loginname) {
		this.loginname = loginname;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Short getShortid() {
		return shortid;
	}

	public void setShortid(Short shortid) {
		this.shortid = shortid;
	}

}
